.. Copyright 2016 Joshua Taillon
..
.. Licensed under the Apache License, Version 2.0 (the "License");
.. you may not use this file except in compliance with the License.
.. You may obtain a copy of the License at
..
..     http://www.apache.org/licenses/LICENSE-2.0
..
.. Unless required by applicable law or agreed to in writing, software
.. distributed under the License is distributed on an "AS IS" BASIS,
.. WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
.. See the License for the specific language governing permissions and
.. limitations under the License.

FIBtracking
===========

**PyPI:** |pypiV| |pypiDM|

**Help!** |rtfd| |gitter|

..  |rtfd| image:: https://readthedocs.org/projects/fibtracking/badge/?version=latest
    :target: http://fibtracking.readthedocs.org/en/latest/?badge=latest
    :alt: Documentation Status

..  |gitter| image:: https://img.shields.io/gitter/room/nwjs/nw.js.svg
    :alt: Join the chat at https://gitter.im/jat255/FIBtracking
    :target: https://gitter.im/jat255/FIBtracking?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge

..  |pypiV| image:: https://img.shields.io/pypi/v/FIBtracking.svg
    :target: https://pypi.python.org/pypi/FIBtracking/

..  |pypiDM| image:: https://img.shields.io/pypi/dm/FIBtracking.svg
    :target: https://pypi.python.org/pypi/FIBtracking/

This package provides methods to track fiducial marks between subsequent slices in a nanotomography run from
the FIB-SEM and allows for calculation of slice thickness based on these fiducials.
Author: Joshua Taillon (jat255@gmail.com)