# Copyright 2016 Joshua Taillon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import re

import cv2


def versioncomp(version1, version2):
    def normalize(v):
        """
        Compares two version numbers
        (from http://stackoverflow.com/a/1714190/1435788)
        """
        v = v.split("-")[0]
        return [int(x) for x in re.sub(r'(\.0+)*$', '', v).split(".")]

    return cmp(normalize(version1), normalize(version2))

if versioncomp(cv2.__version__,'3') < 0:
    raise ImportError("OpenCV version is " + cv2.__version__ + ", but v3.0+ is required.")